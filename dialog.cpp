#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>
#include <QCloseEvent>
#include <QMessageBox>

#include "dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog {parent}
      , modeCombo {new QComboBox()}
      , newButton {new QPushButton(tr("New"))}
      , stepButton {new QPushButton(tr("Step"))}
      , backButton {new QPushButton(tr("Back"))}
      , runButton {new QPushButton(tr("Run"))}
      , closeButton {new QPushButton(tr("Close"))}
      , desk {new Desk(parent)}
      , statusLabel {new QLabel(tr("Initiaize"))}
{
    modeCombo->addItem(tr("Black"));
    modeCombo->addItem(tr("White"));
    //modeCombo->addItem(tr("Manual"));

    stepButton->setEnabled(false);
    backButton->setEnabled(false);
    runButton->setEnabled(false);

    auto mainLayout = new QGridLayout;

    mainLayout->addWidget(modeCombo, 0, 0);
    mainLayout->addWidget(newButton, 0, 1);
    mainLayout->addWidget(stepButton, 0, 2);
    mainLayout->addWidget(backButton, 0, 3);
    mainLayout->addWidget(runButton, 0, 4);
    mainLayout->addWidget(closeButton, 0, 5);
    mainLayout->addWidget(desk, 1, 0, 1, 6);
    mainLayout->addWidget(statusLabel, 2, 0, 1, 6);

    setLayout(mainLayout);

    setWindowTitle(tr("QT Gomoku"));

    connect(closeButton, &QPushButton::clicked, this, &Dialog::close);
    connect(newButton, &QPushButton::clicked, this, &Dialog::newGame);
    connect(desk, &Desk::msgChanged, statusLabel, &QLabel::setText);
    connect(desk, &Desk::gameIsOver, this, &Dialog::gameOver);

    statusLabel->setText(tr("Ready"));

    //newGame();
}

void Dialog::newGame() {

    //if(modeCombo->currentIndex() == 0) {
    //    stepButton->setEnabled(true);
    //} else {
    //    stepButton->setEnabled(false);
    //    backButton->setEnabled(false);
    //}

    modeCombo->setEnabled(false);
    stepButton->setEnabled(false);
    backButton->setEnabled(false);
    runButton->setEnabled(false);

    desk->clear(modeCombo->currentIndex());

}

void Dialog::gameOver() {
    modeCombo->setEnabled(true);
}

void Dialog::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

bool Dialog::maybeSave()
{
    QMessageBox::StandardButton ret;

    ret = QMessageBox::warning(this, tr("QT Gomoku"),
        tr("Do you want to quit?"),
            QMessageBox::Ok | QMessageBox::Cancel);

    if (ret == QMessageBox::Ok) {
        return true;
    } else if (ret == QMessageBox::Cancel) {
        return false;
    }
    return true;
}
