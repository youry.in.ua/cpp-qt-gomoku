#ifndef POINT_H
#define POINT_H

#include <array>
#include <vector>

//#include "net.h"
//#include "slot.h"

//class Net;
class Slot;
enum class Direct;
enum class Color;
enum class Status;

class Point
{
public:
    Point(int x, int y);

    //bool isValidScp(Direct d);
    void addSlot(Slot* slot, Status s);
    //void update(int c);
    int getX() {return x;}
    int getY() {return y;}
    Status getS() {return s;}
    void setS(Status v) {s = v;}
    void incR(Status i) {++r[(int)i];}
    void decR(Status i) {--r[(int)i];}
    int countSlots() {return pslots.size();}
    Slot* getSlot(int i) {return pslots[i];}

private:
    int x;
    int y;
    Status s;
    std::array<int, 3> r;
    std::vector<Slot*> pslots;
};

#endif // POINT_H
