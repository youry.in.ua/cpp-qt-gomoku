#ifndef STEP_H
#define STEP_H


class Step
{
public:
    Step(int x, int y, int c);

    int x;
    int y;
    int c;
};

#endif // STEP_H
