#include <QDebug>

#include "net.h"

Net::Net():
       dirs {Direct::vert, Direct::hor, Direct::up, Direct::down}
{

}

void Net::init(int sz) {

    for(auto s : allSlots) delete s;
    allSlots.clear();
    //for(auto s : activeSlots[0]) delete s;
    activeSlots[0].clear();
    //for(auto s : activeSlots[1]) delete s;
    activeSlots[1].clear();
    //for(auto s : activeSlots[2]) delete s;
    activeSlots[2].clear();

    for(auto p : allPoints) delete p;
    allPoints.clear();
    emptyPoints.clear();

    for(int i = 0; i < sz; ++i)
        for(int j = 0; j < sz; ++j) {
            Point* p = new Point {i, j};
            allPoints.insert(p);
            emptyPoints.insert(p);
        }

    for(auto p : allPoints)
        for(auto d : dirs) {
            if(isValidScp(p, d)) {
                Slot* s = new Slot {p, d};
                allSlots.insert(s);
                activeSlots[(int)Status::free].insert(s);
            }
        }

    for(auto s : allSlots) {
        Direct d = s->getD();
        Point* scp = s->getScp();

        if (d == Direct::vert) {
            s->setPoint(getPoint(scp->getX(), scp->getY() - 2), 0);
            s->setPoint(getPoint(scp->getX(), scp->getY() - 1), 1);
            s->setPoint(getPoint(scp->getX(), scp->getY() + 1), 3);
            s->setPoint(getPoint(scp->getX(), scp->getY() + 2), 4);
        } else if(d == Direct::hor) {
            s->setPoint(getPoint(scp->getX() - 2, scp->getY()), 0);
            s->setPoint(getPoint(scp->getX() - 1, scp->getY()), 1);
            s->setPoint(getPoint(scp->getX() + 1, scp->getY()), 3);
            s->setPoint(getPoint(scp->getX() + 2, scp->getY()), 4);
        } else if(d == Direct::up) {
            s->setPoint(getPoint(scp->getX() - 2, scp->getY() - 2), 0);
            s->setPoint(getPoint(scp->getX() - 1, scp->getY() - 1), 1);
            s->setPoint(getPoint(scp->getX() + 1, scp->getY() + 1), 3);
            s->setPoint(getPoint(scp->getX() + 2, scp->getY() + 2), 4);
        } else if(d == Direct::down) {
            s->setPoint(getPoint(scp->getX() - 2, scp->getY() + 2), 0);
            s->setPoint(getPoint(scp->getX() - 1, scp->getY() + 1), 1);
            s->setPoint(getPoint(scp->getX() + 1, scp->getY() - 1), 3);
            s->setPoint(getPoint(scp->getX() + 2, scp->getY() - 2), 4);
        }

        for(int i = 0; i < 5; ++i) {
            Point* p = s->getPoint(i);
            if(p != NULL)
                p->addSlot(s, Status::free);
        }
    }

}

bool Net::isValidScp(Point* p, Direct d)
{
    int i {p->getX() - 7};
    int j {p->getY() - 7};

    if(d == Direct::vert && j > -6 && j < 6)
        return true;
    if (d == Direct::hor && i > -6 && i < 6)
        return true;
    if (d == Direct::up && (i > -6 && j < 6) && (i < 6 && j > -6))
        return true;
    if (d == Direct::down && (i > -6 && j > -6) && (i < 6 && j < 6))
        return true;
    return false;
}

Point* Net::getPoint(int x, int y)
{
    for(auto p : allPoints) {
        if(p->getX() == x && p->getY() == y)
            return p;
    }
    return NULL;
}

bool Net::checkWin(Color c) {
    for(auto s : activeSlots[(int)colorToStatus(c)]) {
        //qDebug() << (int)c << "Slot rate" << s->getR();
        if (s->getR() > 4) {
            //mes = self.name_c[c] + " :: win!!!"
            qDebug() << (int)c << "win!!";
            return true;
        }
    }
    return false;
}

bool Net::checkDraw() {
    if(activeSlots[(int)Status::free].size() == 0
            && activeSlots[(int)Status::black].size() == 0
            && activeSlots[(int)Status::white].size() == 0) {
        //self.mes = " draw :("
        return true;
    }
    return false;
}

Point* Net::calcPoint(Color c)
{
    Point* p = calc(c);
    //step(p, c);

    return p;
}

Point *Net::calc(Color c)
{
    std::vector<Point*> ret;
    Color cmsg {c};
    Color r {revertColor(c)};
    QString smsg;

    smsg = "findSlot4";
    cmsg = c;
    ret = findSlot4(c);
    if(ret.size() == 0) {
        cmsg = r;
        ret = findSlot4(r);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 2 1";
        cmsg = c;
        ret = findPointX(c, 2, 1);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 2, 1);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 1 5";
        cmsg = c;
        ret = findPointX(c, 1, 5);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 1, 5);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 1 4";
        cmsg = c;
        ret = findPointX(c, 1, 4);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 1, 4);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 1 3";
        cmsg = c;
        ret = findPointX(c, 1, 3);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 1, 3);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 1 2";
        cmsg = c;
        ret = findPointX(c, 1, 2);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 1, 2);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 1 1";
        cmsg = c;
        ret = findPointX(c, 1, 1);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 1, 1);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 0 10";
        cmsg = c;
        ret = findPointX(c, 0, 10);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 0, 10);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 0 9";
        cmsg = c;
        ret = findPointX(c, 0, 9);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 0, 9);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 0 8";
        cmsg = c;
        ret = findPointX(c, 0, 8);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 0, 8);
    }

    if(ret.size() == 0) {
        smsg = "findPointX 0 7";
        cmsg = c;
        ret = findPointX(c, 0, 7);
    }
    if(ret.size() == 0) {
        cmsg = r;
        ret = findPointX(r, 0, 7);
    }

    if(ret.size() == 0) {
        smsg = "calcMaxRate";
        cmsg = c;
        ret = calcMaxRate(c);
    }

    qDebug() << "Calc" << (int)c << (int)cmsg << smsg << ret.size();
    return ret[rand() % ret.size()];
}

void Net::step(Point *p, Color c) {

    emptyPoints.erase(p);

    updatePoint(p, c);

}

std::vector<Point *> Net::calcMaxRate(Color c) {
    std::vector<Point *> ret;

    int r {-1};
    int d {0};
    int i {0};

    for(auto p : emptyPoints) {
        d = 0;
        int n = p->countSlots();
        for(int j = 0; j < n; ++j) {
            Slot* s = p->getSlot(j);
            if(s->getS() == Status::free)
                d += 1;
            else if(s->getS() == colorToStatus(c))
                d += (1 + s->getR()) * (1 + s->getR());
            else if(s->getS() != Status::out)
                d += (1 + s->getR()) * (1 + s->getR());
        }

        if(d > r) {
            i = 1;
            r = d;
            ret.clear();
            //m = "{0} {1} :: point_max_rate({2},{3}) -> ({4},{5})".format(self.mes, self.name_c[c], i, r, p.x, p.y)
            ret.push_back(p);
        }
        else if(d == r) {
            ++i;
            //m = "{0} {1} :: point_max_rate({2},{3}) -> ({4},{5})".format(self.mes, self.name_c[c], i, r, p.x, p.y)
            ret.push_back(p);
        }

    }
    return ret;
}

std::vector<Point*> Net::findSlot4(Color c) {
    std::vector<Point*> ret;

    //int m {0};
    for(auto s : activeSlots[(int)colorToStatus(c)])
        if(s->getR() == 4) {
            for(int i = 0; i < 5; ++i) {
                Point* p = s->getPoint(i);
                if(p->getS() == Status::free) {
                    //m = "{0} {1} :: find_slot_4 -> -> ({2},{3})".format(self.mes, self.name_c[c], p.x, p.y)
                    ret.push_back(p);
                }
            }
        }

    return ret;
}

std::vector<Point *> Net::findPointX(Color c, int r, int b)
{
    std::vector<Point *> ret;
    int i {0};
    int n {0};

    for(auto p : emptyPoints) {
        i = 0;
        n = p->countSlots();
        for(int j = 0; j < n; ++j) {
            Slot* s = p->getSlot(j);
            if(s->getS() == colorToStatus(c) && s->getR() > r)
                ++i;
        }
        if(i > b) {
            //m = "{0} {1} :: find_point_x({2},{3}) -> ({4},{5})".format(self.mes, self.name_c[c], r, b, p.x, p.y)
            ret.push_back(p);
        }
    }
    return ret;
}

void Net::updatePoint(Point* p, Color c)
{
    //qDebug() << "Update" << (int)c << p->getX() << p->getY();
    p->setS(colorToStatus(c));
    int n = p->countSlots();
    for(int i = 0; i < n; ++i) {
        Slot* s = p->getSlot(i);
        if (s->getS() == Status::free) {
            p->decR(Status::free);
            p->incR(colorToStatus(c));
            s->setS(colorToStatus(c));
            s->setR(1);
            activeSlots[(int)Status::free].erase(s);
            activeSlots[(int)colorToStatus(c)].insert(s);
        } else if (s->getS() == colorToStatus(c)) {
            p->incR(colorToStatus(c));
            s->incR();
        } else if(s->getS() != Status::out) {
            p->decR(colorToStatus(c));
            activeSlots[(int)s->getS()].erase(s);
            s->setS(Status::out);
        }
    }
}
