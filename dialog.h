#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

#include "desk.h"

QT_BEGIN_NAMESPACE

class QLabel;
//class QLineEdit;
//class QSpinBox;
class QPushButton;
class QComboBox;
class QWidget;

QT_END_NAMESPACE

class Dialog: public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);

private:
    QComboBox *modeCombo;
    QPushButton *newButton;
    QPushButton *stepButton;
    QPushButton *backButton;
    QPushButton *runButton;
    QPushButton *closeButton;
    Desk *desk;
    QLabel *statusLabel;

    bool maybeSave();
    void newGame();
    void gameOver();

protected:
    void closeEvent(QCloseEvent *event) override;

};

#endif // DIALOG_H
