#ifndef DESK_H
#define DESK_H

#include <QWidget>
#include <vector>

#include "net.h"
#include "step.h"

class Desk : public QWidget
{
    Q_OBJECT

public:
    explicit Desk(QWidget *parent = nullptr);

    void clear(int m);
    void run(bool r);
    void go(bool a);

signals:
    void msgChanged(QString msg);
    void gameIsOver();

public slots:

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    constexpr static int sz = 15;
    constexpr static int d = 40;
    constexpr static int l = d * 16;
    //constexpr static int cx = l/2;
    //constexpr static int cy = l/2;

    int mode {0};

    bool isPlay {false};
    bool isRun  {false};

    std::vector<int> cx;
    std::vector<int> cy;
    std::vector<Step*> steps;

    Net* net;

    QSize sizeHint() const;
    void drawGrid(QPainter& painter);
    void drawSteps(QPainter& painter);

    QString colorToString(Color c) {return c == Color::black ? tr("Black") : tr("White");}

    void nextStep(int x, int y);
    int calcStep();
    void gameOver(QString msg);

    int getDx(int x);
    int getDy(int y);
    bool isEmpty(int x, int y);

};

#endif // DESK_H
