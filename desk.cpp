#include <QMessageBox>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QResizeEvent>
#include <QPainter>
//#include <iostream>
#include <QDebug>

#include "desk.h"

Desk::Desk(QWidget *parent)
    : QWidget {parent}
    , net {new Net {}}
{
    //qDebug()  << "Desk constructor";
    setMinimumSize(QSize {l, l});
    setMaximumSize(QSize {l, l});

    int padx {0};
    int pady {0};

    for(int i = 0; i < sz; ++i) {
        cx.push_back(padx + d * (i + 1));
        cy.push_back(pady + d * (i + 1));
    }

    repaint();
}

void Desk::clear(int m) {

    emit msgChanged(tr("New game. Your color is ") + colorToString((Color)m));

    mode = m;

    for(auto s : steps) delete s;
    steps.clear();

    net->init(sz);

    isPlay = true;
    isRun = false;

    nextStep(7, 7);

    if(mode == 0)
        run(false);
}

void Desk::nextStep(int x, int y) {
    int c = steps.size() % 2;
    qDebug()  << "Next step: " << x << y << c;
    steps.push_back(new Step {x, y, c});
    net->step(net->getPoint(x, y), (Color)c);

    repaint();
}

int Desk::calcStep() {
    int c = (steps.size() - 1) % 2;

    if(net->checkWin((Color)c)) {
        QString msg {colorToString((Color)c) + tr(" win!")};
        emit msgChanged(msg);
        gameOver(msg);
        emit gameIsOver();
        return -1;
    } else if(net->checkDraw()) {
        QString msg {tr("Draw!")};
        emit msgChanged(msg);
        gameOver(msg);
        emit gameIsOver();
        return -1;
    }
    else {
        Point* p = net->calcPoint((Color)(1 - c));
        nextStep(p->getX(), p->getY());
        if(net->checkWin((Color)(1 - c))) {
            QString msg {colorToString((Color)(1 - c)) + tr(" win!")};
            emit msgChanged(msg);
            gameOver(msg);
            emit gameIsOver();
            return -1;
        } else if(net->checkDraw()) {
            QString msg {tr("Draw!")};
            emit msgChanged(msg);
            gameOver(msg);
            emit gameIsOver();
            return -1;
        }
    }
    return steps.size() - 1;
}

void Desk::gameOver(QString msg)
{
    QMessageBox::information(this, tr("QT Gomoku"),
            tr("Game over! ") + msg,
                QMessageBox::Ok);
}

void Desk::run(bool r)
{
    if(isPlay) {
        isRun = r;
        go(true);
    }

}

void Desk::go(bool a)
{
    int ret = calcStep();

    if (ret < 0 || ret > sz * sz - 1) {
        isRun = false;
        isPlay = false;
    }
}

int Desk::getDx(int x)
{
    int r {d/2};
    for(int i = 0; i < cx.size(); ++i) {
        if (x > cx[i] - r && x < cx[i] + r)
            return i;
    }
    return -1;
}

int Desk::getDy(int y)
{
    int r {d/2};
    for(int i = 0; i < cy.size(); ++i) {
        if (y > cy[i] - r && y < cy[i] + r)
            return i;
    }
    return -1;
}

bool Desk::isEmpty(int x, int y)
{
    for(int i = 0; i < steps.size(); ++i) {
        if (steps[i]->x == x && steps[i]->y == y)
            return false;
    }
    return true;
}

void Desk::drawGrid(QPainter& painter) {
    //qDebug()  << "Draw grid";
    //QPainter painter {this};

    painter.setBackground(Qt::lightGray);
    painter.setPen(Qt::darkGray);
    //qDebug()  << "Draw grid";
    for(int i = 0; i < sz; ++i) {
        //qDebug()  << cx[i] << cy[0] << cx[i] << cy[sz - 1];
        painter.drawLine(cx[i], cy[0], cx[i], cy[sz - 1]);
        painter.drawLine(cx[0], cy[i], cx[sz - 1], cy[i]);
    }

}

void Desk::drawSteps(QPainter& painter) {

    //qDebug()  << "Draw steps:" << steps.size() ;
    for(int i = 0; i < (int)steps.size(); ++i) {
        int r {d/2};
        int x {cx[steps[i]->x] - r};
        int y {cy[steps[i]->y] - r};
        //qDebug()  << "Draw step:" << i << x << y ;
        painter.setPen(Qt::black);
        if(steps[i]->c == 0) {
            painter.setBrush(QBrush {Qt::black});
        } else {
            painter.setBrush(QBrush {Qt::white});
        }
        painter.drawEllipse(x, y, d, d);
        if(steps[i]->c == 0) {
            painter.setPen(Qt::white);
        } else {
            painter.setPen(Qt::black);
        }
        painter.drawText(x, y, d, d, Qt::AlignCenter, QString::number(i + 1));
    }
}

void Desk::mousePressEvent(QMouseEvent *event)
{
    if (isPlay && event->button() == Qt::LeftButton) {
        //qDebug()  << "Mouse press:" <<  event->pos();
        int x = getDx(event->pos().x());
        int y = getDy(event->pos().y());

        if(x > -1 && y > - 1) {
            if(isEmpty(x, y)) {
                nextStep(x, y);
                go(false);
                //repaint();
            }
        }
    }
}

void Desk::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        //drawLineTo(event->pos());
    }
}

void Desk::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        //drawLineTo(event->pos());
        //scribbling = false;
    }
}

void Desk::paintEvent(QPaintEvent *event)
{
    QPainter painter {this};
    //QRect dirtyRect = event->rect();
    //painter.drawImage(dirtyRect, image, dirtyRect);
    drawGrid(painter);
    drawSteps(painter);
}

void Desk::resizeEvent(QResizeEvent *event)
{
    //if (width() > image.width() || height() > image.height()) {
    //    int newWidth = qMax(width() + 128, image.width());
    //    int newHeight = qMax(height() + 128, image.height());
    //    resizeImage(&image, QSize(newWidth, newHeight));
    //    update();
    //}
    QWidget::resizeEvent(event);
}

QSize Desk::sizeHint() const {
    return QSize {l, l};
}

