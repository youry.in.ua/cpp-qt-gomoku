#ifndef NET_H
#define NET_H

#include <vector>
#include <array>
#include <set>
#include <cstdlib>

#include "slot.h"
#include "point.h"

enum class Direct {vert, hor, up, down};
enum class Color {black, white};
enum class Status {free, black, white, out};

class Net
{
public:
    Net();

    void init(int sz);
    Point* getPoint(int x, int y);
    Color revertColor(Color c) {return c == Color::black ? Color::white : Color::black;}
    Status colorToStatus(Color c) {return c == Color::black ? Status::black : Status::white;}
    bool checkWin(Color c);
    bool checkDraw();
    Point* calcPoint(Color c);
    void step(Point *p, Color c);

private:
    std::array<Direct, 4> dirs;
    std::set<Slot*> allSlots;
    std::array<std::set<Slot*>, 3> activeSlots;
    std::set<Point*> allPoints;
    std::set<Point*> emptyPoints;

    bool isValidScp(Point* p, Direct d);
    Point* calc(Color c);

    std::vector<Point*> calcMaxRate(Color c);
    std::vector<Point*> findSlot4(Color c);
    std::vector<Point*> findPointX(Color c, int r, int b);

    void updatePoint(Point* p, Color c);

};

#endif // NET_H
