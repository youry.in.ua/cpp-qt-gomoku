#ifndef SLOT_H
#define SLOT_H

#include <array>

//#include "net.h"
//#include "point.h"

//class Net;
class Point;

enum class Direct;
enum class Color;
enum class Status;

class Slot
{
public:
    Slot(Point* scp, Direct d);

    void init();
    Point* getScp() {return scp;}
    Status getS() {return s;}
    void setS(Status v) {s = v;}
    int getR() {return r;}
    void setR(int v) {r = v;}
    void incR() {++r;}
    Direct getD() {return d;}
    Point* getPoint(int i) {return points[i];}
    void setPoint(Point *p, int i);

private:
    Point* scp;
    Direct d;
    int r;
    Status s;
    std::array<Point*, 5> points;
};

#endif // SLOT_H
