#include "point.h"

Point::Point(int x, int y):
    x {x}, y {y}, s {(Status)0}, r {{0, 0, 0}}
{

}

void Point::addSlot(Slot *slot, Status s)
{
    pslots.push_back(slot);
    ++r[(int)s];
}

